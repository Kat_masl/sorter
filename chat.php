<?php

//...

$host = "localhost";  
$user = "user_bd";
$pass = "field234512.K-b"; 
$db_name = "sorter"; 

$connect = mysqli_connect($host, $user, $pass, $db_name);

//Проверка на ошибку
if (!$connect) {
    die('Error connect!');//Выводит сообщение и останавливает выполнение кода, который идет дальше (die)
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Chat</title>
</head>
<body>
    <div class="col-12 d-flex flex-column align-items-center">
        <h1 class="text-center">Чат</h1>
        <form class="d-flex flex-column nx-3 flex-2 col-2 align-items-left" action="search.php" method="post">
            <div class="mb-3">
                <input type="text" name="valueToSearch" class="form-control n-2 mb-3" placeholder="Поиск #">
                <input class="btn btn-dark n-2 " name="search" type="submit" value="Filter">
                <a href="index.php" class="btn btn-danger ">Добавить сообщение</a>
            </div>
        </form>
        <?php
            //Делаем выборку всех строк из таблицы "sms"
            $cards = mysqli_query($connect, "SELECT * FROM `sms`");
            //Преобразовываем полученные данные в нормальный массив
            $cards = mysqli_fetch_all($cards);
            // Перебираем массив и рендерим HTML с данными из массива
            foreach ($cards as $card) {
        ?>
        <div class="card mb-3 col-5 ">
            <div class="card-header">
                Карта
            </div>
            <div class="card-body">
                <p class="card-text">Номер: <?= $card[0] ?></p>
                <h5 class="card-title">Канал: <?= $card[1] ?></h5>
                <p class="card-text">Сообщение: <?= $card[2] ?></p>
                <a href="update.php?id=<?= $card[0] ?>" class="btn btn-dark">Изменить</a>
            </div>
        </div>
        <?php
            }
        ?>
    </div>
</body>
</html>