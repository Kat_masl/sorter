<?php

$host = "localhost";  
$user = "user_bd";
$pass = "field234512.K-b"; 
$db_name = "sorter"; 

$connect = mysqli_connect($host, $user, $pass, $db_name);

//Проверка на ошибку
if (!$connect) {
    die('Error connect!');//Выводит сообщение и останавливает выполнение кода, который идет дальше (die)
}

//Получаем ID продукта из адресной строки - /product.php?id=1

$card_id = $_GET['id'];

//Делаем выборку строки с полученным ID выше

$product = mysqli_query($connect, "SELECT * FROM `sms` WHERE `id` = '$card_id'");

//Преобразовывем полученные данные в нормальный массив Используя функцию mysqli_fetch_assoc массив будет иметь ключи равные названиям столбцов в таблице

$product = mysqli_fetch_assoc($product);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Update</title>
</head>
<body>
    <div class="col-12 d-flex flex-column align-items-center">
        <h1 class="text-center">Изменение</h1>
        <form action="update1.php" class="d-flex flex-column nx-2 flex-5 col-4" name="SMS-form" method="post">
        <input type="hidden" name="id" value="<?= $product['id'] ?>">
            <div class="mb-3">
                <label class="nx-2" for="channel" class="form-label">Канал</label>
                <input name="name" type="text" class="form-control n-2" id="channel" value="<?= $product['name'] ?>" placeholder="Введите название канала" required>
            </div>
            <div class="mb-3">
                <label class="nx-2" for="disabledTextInput" class="form-label">Сообщение</label>
                <input name="content" type="text" class="form-control n-2" id="disabledTextInput" value="<?= $product['content'] ?>" placeholder="Введите сообщение с #" required>
            </div>
            <div class="d-grid gap-2 col-12 mx-auto mb-3">
                <button class="btn btn-dark" type="submit">Изменить</button>
            </div>
            <div class="d-grid gap-2 col-12 mx-auto">
            <a href="chat.php" class="btn btn-danger ">Не хочу изменять</a>
            </div>
        </form>
    </div>
</body>