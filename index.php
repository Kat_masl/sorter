<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Sorter</title>
</head>
<body>
    <div class="col-12 d-flex flex-column align-items-center">
        <h1 class="text-center">Сортер #</h1>
        <form action="admessage.php" class="d-flex flex-column nx-2 flex-5 col-4" name="SMS-form" method="post">
            <div class="mb-3">
                <label class="nx-2" for="canal" class="form-label">Номер</label>
                <input name="id" type="text" class="form-control n-2" id="canal" placeholder="Введите номер сообщения" required>
            </div>
            <div class="mb-3">
                <label class="nx-2" for="channel" class="form-label">Канал</label>
                <input name="name" type="text" class="form-control n-2" id="channel" placeholder="Введите название канала" required>
            </div>
            <div class="mb-3">
                <label class="nx-2" for="text" class="form-label">Сообщение</label>
                <input name="content" type="text" class="form-control n-2" id="text" placeholder="Введите сообщение с #" required>
            </div>
            <div class="d-grid gap-2 col-12 mx-auto">
                <button class="btn btn-dark" type="submit">Отправить</button>
                <a class="btn btn-danger" href="chat.php" role="button">Чат</a>
            </div>
        </form>
    </div>
</body>
